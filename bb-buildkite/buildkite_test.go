package bb_buildkite

import (
    "encoding/json"
    "net/http"
    "net/http/httptest"
    "net/url"
    "reflect"
    "testing"

    "github.com/buildkite/go-buildkite/buildkite"
    "github.com/stretchr/testify/assert"
    "fmt"
)

func setup() (client *buildkite.Client, mux *http.ServeMux, serverURL string, teardown func()) {
    mux = http.NewServeMux()
    server := httptest.NewServer(mux)
    client = buildkite.NewClient(http.DefaultClient)
    sUrl, _ := url.Parse(server.URL)
    client.BaseURL = sUrl
    return client, mux, server.URL, server.Close
}

func TestNewClient(t *testing.T) {
    config, _ := buildkite.NewTokenConfig("sasa", false)
    b := BBBuildkite{buildkite.NewClient(config.Client()), nil}

    type args struct {
        token string
    }
    tests := []struct {
        name  string
        args  args
        wantB BBBuildkite
    }{
        {"WithToken", args{"sasa"}, b},
        {"NoToken", args{""}, BBBuildkite{}},
    }
    for _, tt := range tests {
        t.Run(tt.name, func(t *testing.T) {
            if gotB := newClient(tt.args.token); !reflect.DeepEqual(gotB, tt.wantB) {
                t.Errorf("NewClient() = %v, want %v", gotB, tt.wantB)
            }
        })
    }
}

func TestBBBuildkite_Config(t *testing.T) {
    emptyBB := BBBuildkite{}
    wantBB := BBBuildkite{}
    jsonAsStr := ` {
        "pipelineConfig" : {
            "name":"TestAddPipeLineViaREST",
            "repository":"git@bitbucket.org:blackbeltdevops/nexus-reposize-plugin.git",
            "steps": [
                {
                    "type": "script",
                    "name": "Build :hammer:",
                    "command": "buildkite-agent pipeline upload"
                }
            ]
        }
    }`
    json.Unmarshal([]byte(jsonAsStr), &wantBB)
    assert.Equal(t, "TestAddPipeLineViaREST", wantBB.CreatePipeline.Name, "no name for CreatePipeline")
    type args struct {
        jsonAsStr string
    }
    tests := []struct {
        name string
        b    BBBuildkite
        args args
        want BBBuildkite
    }{
        {"emptyConfig", emptyBB, args{""}, emptyBB},
        {"emptyConfig", emptyBB, args{jsonAsStr}, wantBB},
    }
    for _, tt := range tests {
        t.Run(tt.name, func(t *testing.T) {
            if got := tt.b.Config(tt.args.jsonAsStr); !reflect.DeepEqual(got, tt.want) {
                t.Errorf("BBBuildkite.Config() = %v, want %v", got, tt.want)
            }
        })
    }
}

func TestBBBuildkite_PipelinesCreate(t *testing.T) {
    cli, mux, _, teardown := setup()
    defer teardown()

    testOrg := "BlackBeltTechnology"

    mux.HandleFunc("/v2/organizations/"+testOrg+"/pipelines",
        func(w http.ResponseWriter, r *http.Request) {
            w.WriteHeader(http.StatusNoContent)
        },
    )

    jsonAsStr := ` {
        "pipelineConfig" : {
            "name":"TestAddPipeLineViaREST",
            "repository":"git@bitbucket.org:blackbeltdevops/nexus-reposize-plugin.git",
            "steps": [
                {
                    "type": "script",
                    "name": "Build :hammer:",
                    "command": "buildkite-agent pipeline upload"
                }
            ]
        }
    }`

    emptyBB := BBBuildkite{Client: cli}
    fakeBB := BBBuildkite{Client: cli}
    json.Unmarshal([]byte(jsonAsStr), &fakeBB)

    assert.Equal(t, "TestAddPipeLineViaREST", fakeBB.Name, " Nope ")

    fakePipelineCreate := BBBuildkite{Client: cli}
    json.Unmarshal([]byte(jsonAsStr), &fakePipelineCreate)
    pipe, resp, _ := fakePipelineCreate.Pipelines.Create(testOrg, fakePipelineCreate.CreatePipeline)

    wantFailPipe := buildkite.Pipeline{}

    type args struct {
        org string
    }
    tests := []struct {
        name     string
        b        BBBuildkite
        args     args
        wantPipe *buildkite.Pipeline
        wantResp *buildkite.Response
        wantErr  bool
    }{
        {
            "success", fakeBB, args{testOrg}, pipe, resp, true,
        },
        {
            "configNotFound", emptyBB, args{testOrg}, &wantFailPipe,
            &buildkite.Response{Response: &http.Response{StatusCode: 400}}, true,
        },
    }
    for _, tt := range tests {
        t.Run(tt.name, func(t *testing.T) {
            gotPipe, gotResp, err := tt.b.PipelinesCreate(tt.args.org)
            if (err != nil) != tt.wantErr {
                t.Errorf("BBBuildkite.PipelinesCreate() error = %v, wantErr %v", err, tt.wantErr)
                return
            }
            if !reflect.DeepEqual(gotPipe, tt.wantPipe) {
                t.Errorf("BBBuildkite.PipelinesCreate() gotPipe = %v, want %v", gotPipe, tt.wantPipe)
            }
            if !reflect.DeepEqual(gotResp.StatusCode, tt.wantResp.StatusCode) {
                t.Errorf("BBBuildkite.PipelinesCreate() gotResp.StatusCode = %v, want %v", gotResp.StatusCode, tt.wantResp.StatusCode)
            }
            if !reflect.DeepEqual(gotResp.Body, tt.wantResp.Body) {
                t.Errorf("BBBuildkite.PipelinesCreate() gotResp.Body = %v, want %v", gotResp.Body, tt.wantResp.Body)
            }
        })
    }
}

func TestBBBuildkite_ListWebHook(t *testing.T) {
    cli, mux, _, teardown := setup()
    defer teardown()

    testOrg := "BlackBeltTechnology"

    mux.HandleFunc("/v2/organizations/"+testOrg+"/pipelines", func(w http.ResponseWriter, r *http.Request) {
        fmt.Fprint(w, `[{"id":"123"},{"id":"1234","provider": { "id": "1234","webhook_url": "1234" } } ]`)
    })

    emptyBB := BBBuildkite{Client: cli}
    fakeId := buildkite.String("1234")
    wantPipe := []buildkite.Pipeline{{ID: buildkite.String("123")}, {ID: fakeId, Provider: &buildkite.Provider{ID: fakeId, WebhookURL: fakeId}}}

    type fields struct {
        Client         *buildkite.Client
        CreatePipeline *buildkite.CreatePipeline
    }
    type args struct {
        org string
    }
    tests := []struct {
        name     string
        fields   fields
        args     args
        wantPipe []buildkite.Pipeline
        wantResp *buildkite.Response
        wantErr  bool
    }{
        {"success", fields{emptyBB.Client,
            emptyBB.CreatePipeline}, args{testOrg},
            wantPipe, &buildkite.Response{Response: &http.Response{StatusCode: 200}}, false},
    }
    for _, tt := range tests {
        t.Run(tt.name, func(t *testing.T) {
            b := BBBuildkite{
                Client:         tt.fields.Client,
                CreatePipeline: tt.fields.CreatePipeline,
            }
            gotPipe, gotResp, err := b.ListWebHook(tt.args.org)
            if (err != nil) != tt.wantErr {
                t.Errorf("BBBuildkite.ListWebHook() error = %v, wantErr %v", err, tt.wantErr)
                return
            }
            if !reflect.DeepEqual(gotPipe, tt.wantPipe) {
                t.Errorf("BBBuildkite.ListWebHook() gotPipe = %v, want %v", gotPipe, tt.wantPipe)
            }
            if !reflect.DeepEqual(gotResp.StatusCode, tt.wantResp.StatusCode) {
                t.Errorf("BBBuildkite.ListWebHook() gotResp = %v, want %v", gotResp, tt.wantResp)
            }
        })
    }
}
