package bb_buildkite

import (
    "encoding/json"
    "errors"
    "fmt"

    "github.com/buildkite/go-buildkite/buildkite"
    "net/http"
)

type BBBuildkite struct {
    *buildkite.Client
    *buildkite.CreatePipeline `json:"pipelineConfig"`
}

func newClient(token string) (b BBBuildkite) {
    config, err := buildkite.NewTokenConfig(token, false)
    if err != nil {
        fmt.Printf("client config failed: %s", err)
        b = BBBuildkite{}
        return
    }
    b = BBBuildkite{buildkite.NewClient(config.Client()), nil,}
    return
}

func (b BBBuildkite) Config(jsonAsStr string) BBBuildkite {
    json.Unmarshal([]byte(jsonAsStr), &b)
    return b
}

func (b BBBuildkite) ConfigV2(bCP *buildkite.CreatePipeline) BBBuildkite {
    b.CreatePipeline = bCP
    return b
}

func (b BBBuildkite) PipelinesCreate(org string) (pipe *buildkite.Pipeline, resp *buildkite.Response, err error) {
    if b.CreatePipeline == nil {
        err = errors.New("config not found")
        return &buildkite.Pipeline{}, &buildkite.Response{Response: &http.Response{StatusCode: 400}}, err
    }
    pipe, resp, err = b.Pipelines.Create(org, b.CreatePipeline)
    return
}

func (b BBBuildkite) ListWebHook(org string) (pipe []buildkite.Pipeline, resp *buildkite.Response, err error) {
    pipe, resp, err = b.Pipelines.List(org, nil)
    return
}
