ARG SRC_DIR=src/bb-buildkite-api
ARG LIB_DIR=bb-buildkite

# build stage
FROM iron/go:dev AS build-env
ARG SRC_DIR
ARG LIB_DIR
WORKDIR /go
COPY .  $SRC_DIR
RUN cd  $SRC_DIR/$LIB_DIR && go get -t -d && CGO_ENABLED=0 GOOS=linux  go test -a -installsuffix cgo -o test -run=nope -v

# final stage#
FROM alpine
ARG SRC_DIR
ARG LIB_DIR
WORKDIR /app
COPY --from=build-env /go/$SRC_DIR/$LIB_DIR /app/
#RUN /app/test -test.v
CMD ["/app/test", "-test.v"]
