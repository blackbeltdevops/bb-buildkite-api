# bb-buildkite-api

client := bb_buildkite.NewClient("7524e2ba9c9f418")

org := "blackbelt-technology-1"

piTestData := `{
    "name":"TestAddPipeLineViaREST",
    "repository":"git@bitbucket.org:blackbeltdevops/nexus-reposize-plugin.git",
    "steps": [
        {
            "type": "script",
            "name": "Build :hammer:",
            "command": "buildkite-agent pipeline upload"
        }
    ]
}`

b, _, _ := client.Builds.List(nil)
fmt.Println(b)

pipe, resp, err := client.Config(piTestData).PipelinesCreate(org)
fmt.Println(pipe)
fmt.Println(resp)
fmt.Println(err)